const express = require('express')
const router = express.Router()

const product = [
  {
    id: 1001,
    name: 'Node.js for Beginners',
    category: 'Node',
    price: 990
  },
  {
    id: 1002,
    name: 'React 101',
    category: 'React',
    price: 3990
  },
  {
    id: 1003,
    name: 'Getting started with MongoDB',
    category: 'MongoDB',
    price: 1990
  }
]

router.get('/', (req, res, next) => {
  res.json(product)
})

router.get('/:id', (req, res, next) => {
  console.log(req.params)
  const { id } = req.params
  const result = product.find(product => product.id === parseInt(id))
  res.json(result)
})
router.post('/', (req, res, next) => {
  const payload = req.body
  product.push(payload)
  res.json(payload)
})
router.put('/', (req, res, next) => {
  const payload = req.body
  const index = product.findIndex(product => product.id ===payload.id)
  console.log(index)
  product.splice(index, 1, payload)
  res.json(payload)
})

router.delete('/:id', (req, res, next) => {
  const { id } = req.params
  const index = product.findIndex(product => product.id === id)
  console.log(index)
  product.splice(index, 0)
  res.json({ id })
})

module.exports = router